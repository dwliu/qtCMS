# 数据表：node 结构信息
DROP TABLE IF EXISTS `qt_node`;
CREATE TABLE `qt_node` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` int(11) NOT NULL COMMENT '父节点id',
  `name` varchar(32) NOT NULL COMMENT '节点名称',
  `title` varchar(32) NOT NULL COMMENT '节点标题',
  `level` tinyint(4) NOT NULL COMMENT '节点等级',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '节点状态',
  `created_at` int(11) NOT NULL COMMENT '创建时间',
  `updated_at` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='节点表' AUTO_INCREMENT=0 ;

# 数据库表：role 结构信息
DROP TABLE IF EXISTS `qt_role`;
CREATE TABLE `qt_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` int(11) NOT NULL COMMENT '父角色id',
  `name` varchar(32) NOT NULL COMMENT '角色名称',
  `description` text NOT NULL COMMENT '角色描述',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '角色状态',
  `created_at` int(11) NOT NULL COMMENT '创建时间',
  `updated_at` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='角色表' AUTO_INCREMENT=0 ;

# 数据库表：admin 结构信息
DROP TABLE IF EXISTS `qt_admin`;
CREATE TABLE `qt_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `role_id` int(11) NOT NULL COMMENT '所属角色id',
  `email` varchar(64) NOT NULL COMMENT '登录邮箱',
  `password` varchar(32) NOT NULL COMMENT '登录密码',
  `mail_hash` varchar(36) NOT NULL COMMENT '邮件hash值',
  `remark` text NOT NULL COMMENT '管理员备注信息',
  `is_super` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否超级管理员',
  `is_active` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否启用',
  `last_login_at` int(11) NOT NULL COMMENT '最后登录时间',
  `created_at` int(11) NOT NULL COMMENT '创建时间',
  `updated_at` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`email`),
  KEY `fk_admin_role` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='管理员表' AUTO_INCREMENT=0 ;

# 数据库表：role_admin 结构信息
DROP TABLE IF EXISTS `qt_role_admin`;
CREATE TABLE `qt_role_admin` (
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `user_id` int(11) NOT NULL COMMENT '管理员id',
  KEY `fk_role_admin` (`role_id`),
  KEY `fk_admin_role` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员权限表';

# 数据库表：access 结构信息
DROP TABLE IF EXISTS `qt_access`;
CREATE TABLE `qt_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `node_id` int(11) NOT NULL COMMENT '节点id',
  PRIMARY KEY (`id`),
  KEY `fk_role_access` (`role_id`),
  KEY `fk_node_acess` (`node_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='结点权限访问表' AUTO_INCREMENT=0 ;


-- --
-- -- 限制表 `qt_access`
-- --
ALTER TABLE `qt_access`
  ADD CONSTRAINT `fk_node_acess` FOREIGN KEY (`node_id`) REFERENCES `qt_node` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_role_acess` FOREIGN KEY (`role_id`) REFERENCES `qt_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- --
-- -- 限制表 `qt_admin`
-- --
ALTER TABLE `qt_admin`
  ADD CONSTRAINT `fk_admin_role` FOREIGN KEY (`role_id`) REFERENCES `qt_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- --
-- -- 限制表 `qt_role_admin`
-- --
ALTER TABLE `qt_role_admin`
  ADD CONSTRAINT `fk_ar` FOREIGN KEY (`user_id`) REFERENCES `qt_admin` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ra` FOREIGN KEY (`role_id`) REFERENCES `qt_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

# Role数据

INSERT INTO `qt_role` (`id`, `pid`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, '超级管理员', '系统内置超级管理员组，不受权限分配账号限制', 1, UNIX_TIMESTAMP(), UNIX_TIMESTAMP());




CREATE TABLE `qt_article` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(200) NULL DEFAULT NULL,
	`content` MEDIUMTEXT NULL COMMENT '内容',
	`come_from` VARCHAR(200) NULL DEFAULT NULL COMMENT '来源',
	`category_id` INT(11) NULL DEFAULT NULL COMMENT '栏目id',
	`tag` VARCHAR(100) NULL DEFAULT NULL COMMENT 'tag标签，关键字',
	`is_slide` TINYINT(1) NULL DEFAULT NULL COMMENT '首页幻灯片 1：是，2不是',
	`public_time` INT(11) NULL DEFAULT NULL COMMENT '发布时间',
	`update_time` INT(11) NULL DEFAULT NULL COMMENT '修改时间',
	`has_img` TINYINT(4) NULL DEFAULT NULL COMMENT '是否有图片 1：是，2不是',
	`has_vedio` TINYINT(4) NULL DEFAULT NULL COMMENT '是否有视频 1：是，2不是',
	`target_type` VARCHAR(10) NULL DEFAULT NULL COMMENT '链接跳转类型',
	`open_comment` INT(1) NULL DEFAULT NULL COMMENT '1：开启评论  2：关闭评论',
	PRIMARY KEY (`id`),
	INDEX `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='文章' AUTO_INCREMENT=0;

CREATE TABLE `qt_attachment` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`article_id` INT(10) NULL DEFAULT NULL,
	`attach_type` TINYINT(1) NULL DEFAULT NULL COMMENT '类型： 1图片，2视频',
	`url` VARCHAR(200) NULL DEFAULT NULL COMMENT '文件路径',
	PRIMARY KEY (`id`),
	INDEX `article_id` (`article_id`)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='文章附件' AUTO_INCREMENT=0;

CREATE TABLE `qt_category` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(30) NULL DEFAULT NULL,
	`pid` INT(11) NULL DEFAULT NULL COMMENT '父ID',
	`is_show` INT(1) NULL DEFAULT NULL COMMENT '是否显示1:显示 ，2不显示',
	`sort` INT(3) NULL DEFAULT NULL COMMENT '排序',
	`page_type` INT(1) NULL DEFAULT NULL COMMENT '1:单页面，2:列表页面',
	`static_type` INT(1) NULL DEFAULT NULL COMMENT '1:不可修改和删除，2动态的',
	`target_tpl` VARCHAR(100) NULL DEFAULT NULL COMMENT '对应的模版：single，loop，detail',
	`target_detail_tpl` VARCHAR(100) NULL DEFAULT NULL COMMENT '多页面类型时对应的详细页面模版',
	`relation_model` VARCHAR(20) NULL DEFAULT NULL COMMENT '对应的模型model',
	`page_size` TINYINT(4) NULL DEFAULT NULL COMMENT '分页列表数',
	`open_comment` INT(1) NULL DEFAULT NULL COMMENT '1：开启评论  2：关闭评论',
	PRIMARY KEY (`id`),
	INDEX `pid` (`pid`)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='栏目分类' AUTO_INCREMENT=0;

CREATE TABLE `qt_comment` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`user_id` INT(10) NULL DEFAULT NULL,
	`relation_model` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Article,Product',
	`relation_id` INT(10) NULL DEFAULT NULL COMMENT '被评论的ID',
	`content` VARCHAR(1000) NULL DEFAULT NULL COMMENT '评论内容',
	`public_time` INT(11) NULL DEFAULT NULL COMMENT '发布时间',
	`update_time` INT(11) NULL DEFAULT NULL COMMENT '更新时间',
	PRIMARY KEY (`id`)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='评论' AUTO_INCREMENT=0;

CREATE TABLE `qt_product` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(32) NULL DEFAULT NULL,
	`tag` VARCHAR(100) NULL DEFAULT NULL COMMENT '标签',
	`price` DOUBLE(10,2) NULL DEFAULT NULL COMMENT '原价',
	`discount` DOUBLE(10,2) NULL DEFAULT NULL COMMENT '几折',
	`summary` MEDIUMTEXT NULL COMMENT '概述',
	`content` MEDIUMTEXT NULL COMMENT '详情',
	`category_id` INT(10) NULL DEFAULT NULL,
	`public_time` INT(11) NULL DEFAULT NULL,
	`update_time` INT(11) NULL DEFAULT NULL,
	`recommend` INT(1) NULL DEFAULT NULL COMMENT '1推荐 2不推荐',
	`has_picture` INT(1) NULL DEFAULT NULL COMMENT '1有产品图片， 2没有',
	`main_picture` VARCHAR(100) NULL DEFAULT NULL COMMENT '主图路径',
	PRIMARY KEY (`id`),
	INDEX `category_id` (`category_id`)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='产品' AUTO_INCREMENT=0;

CREATE TABLE `qt_product_image` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`path` VARCHAR(150) NULL DEFAULT NULL,
	`product_id` INT(11) NULL DEFAULT NULL,
	`type` INT(1) NULL DEFAULT NULL COMMENT '1:主图(封面)  2：附图',
	PRIMARY KEY (`id`),
	INDEX `product_id` (`product_id`)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='产品图片' AUTO_INCREMENT=0;

CREATE TABLE `qt_siteinfo` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`seo_keywords` VARCHAR(200) NULL DEFAULT NULL,
	`seo_description` VARCHAR(500) NULL DEFAULT NULL,
	`icp` VARCHAR(100) NULL DEFAULT NULL,
	`weixin` VARCHAR(200) NULL DEFAULT NULL,
	`weibo` VARCHAR(200) NULL DEFAULT NULL,
	`twocode` VARCHAR(200) NULL DEFAULT NULL COMMENT '二维码',
	`company_name` VARCHAR(200) NULL DEFAULT NULL,
	`company_address` VARCHAR(200) NULL DEFAULT NULL,
	`company_tel` VARCHAR(50) NULL DEFAULT NULL,
	`company_fox` VARCHAR(50) NULL DEFAULT NULL,
	`company_website` VARCHAR(100) NULL DEFAULT NULL,
	`company_email` VARCHAR(100) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='网站信息设置' AUTO_INCREMENT=0;

CREATE TABLE `qt_user` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(50) NULL DEFAULT NULL,
	`password` VARCHAR(30) NULL DEFAULT NULL,
	`sex` VARCHAR(4) NULL DEFAULT NULL COMMENT '男，女',
	`reallyname` VARCHAR(30) NULL DEFAULT NULL COMMENT '真实姓名',
	`tel` VARCHAR(30) NULL DEFAULT NULL COMMENT '电话',
	`photo` VARCHAR(100) NULL DEFAULT NULL COMMENT '头像',
	`last_logintime` INT(11) NULL DEFAULT NULL COMMENT '登录时间',
	`public_time` INT(11) NULL DEFAULT NULL COMMENT '注册时间',
	`update_time` INT(11) NULL DEFAULT NULL COMMENT '修改时间',
	`state` INT(1) NULL DEFAULT NULL COMMENT '审核状态',
	PRIMARY KEY (`id`)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='用户' AUTO_INCREMENT=0;

