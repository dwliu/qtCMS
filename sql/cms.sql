-- MySQL dump 10.13  Distrib 5.1.55, for Win32 (ia32)
--
-- Host: localhost    Database: cms
-- ------------------------------------------------------
-- Server version	5.1.55-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `qt_access`
--

DROP TABLE IF EXISTS `qt_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qt_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `node_id` int(11) NOT NULL COMMENT '节点id',
  PRIMARY KEY (`id`),
  KEY `fk_role_access` (`role_id`),
  KEY `fk_node_acess` (`node_id`),
  CONSTRAINT `fk_node_acess` FOREIGN KEY (`node_id`) REFERENCES `qt_node` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_role_acess` FOREIGN KEY (`role_id`) REFERENCES `qt_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='结点权限访问表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qt_access`
--

LOCK TABLES `qt_access` WRITE;
/*!40000 ALTER TABLE `qt_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `qt_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qt_admin`
--

DROP TABLE IF EXISTS `qt_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qt_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `role_id` int(11) NOT NULL COMMENT '所属角色id',
  `email` varchar(64) NOT NULL COMMENT '登录邮箱',
  `password` varchar(32) NOT NULL COMMENT '登录密码',
  `mail_hash` varchar(36) NOT NULL COMMENT '邮件hash值',
  `remark` text NOT NULL COMMENT '管理员备注信息',
  `is_super` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否超级管理员',
  `is_active` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否启用',
  `last_login_at` int(11) NOT NULL COMMENT '最后登录时间',
  `created_at` int(11) NOT NULL COMMENT '创建时间',
  `updated_at` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`email`),
  KEY `fk_admin_role` (`role_id`),
  CONSTRAINT `fk_admin_role` FOREIGN KEY (`role_id`) REFERENCES `qt_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='管理员表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qt_admin`
--

LOCK TABLES `qt_admin` WRITE;
/*!40000 ALTER TABLE `qt_admin` DISABLE KEYS */;
INSERT INTO `qt_admin` VALUES (1,1,'admin','42ec33083e5d707fd8169e588ce21969','','超级管理员',1,1,1421039736,1415234982,1415340724);
/*!40000 ALTER TABLE `qt_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qt_article`
--

DROP TABLE IF EXISTS `qt_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qt_article` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `content` mediumtext COMMENT '内容',
  `come_from` varchar(200) DEFAULT NULL COMMENT '来源',
  `category_id` int(11) DEFAULT NULL COMMENT '栏目id',
  `tag` varchar(100) DEFAULT NULL COMMENT 'tag标签，关键字',
  `is_slide` tinyint(1) DEFAULT NULL COMMENT '首页幻灯片 1：是，2不是',
  `public_time` int(11) DEFAULT NULL COMMENT '发布时间',
  `update_time` int(11) DEFAULT NULL COMMENT '修改时间',
  `has_img` tinyint(4) DEFAULT NULL COMMENT '是否有图片 1：是，2不是',
  `has_vedio` tinyint(4) DEFAULT NULL COMMENT '是否有视频 1：是，2不是',
  `target_type` varchar(10) DEFAULT NULL COMMENT '链接跳转类型',
  `open_comment` int(1) DEFAULT NULL COMMENT '1：开启评论  2：关闭评论',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='文章';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qt_article`
--

LOCK TABLES `qt_article` WRITE;
/*!40000 ALTER TABLE `qt_article` DISABLE KEYS */;
INSERT INTO `qt_article` VALUES (3,'打造综合能源资源服务中心长标题要长长长','从上世纪90年代初创立至今，深圳能源集团妈湾电厂已经走过20年。当时深圳电力紧缺，经常处于开三停四的状态，经济发展急需电力的支撑。妈湾电厂fsgsgsdfggsdfgsdgsdfgsdfgsdfgg','本站',13,NULL,2,1417760472,1421039979,2,NULL,'_self',1),(5,'打造综合能源资源服务中心1','从上世纪90年代初创立至今，深圳能源集团妈湾电厂已经走过20年。当时深圳电力紧缺，经常处于开三停四的状态，经济发展急需电力的支撑。妈湾电厂','本站',13,NULL,NULL,1417760472,NULL,NULL,NULL,NULL,NULL),(6,'打造综合能源资源服务中心2','从上世纪90年代初创立至今，深圳能源集团妈湾电厂已经走过20年。当时深圳电力紧缺，经常处于开三停四的状态，经济发展急需电力的支撑。妈湾电厂','本站',13,NULL,NULL,1417760472,NULL,NULL,NULL,NULL,NULL),(8,'集团主持召开“双三角”能源企业环保工作交流研讨会','<p>\r\n	<p>\r\n		<img src=\"/qtCMS/Public/uploads/image/20141210/20141210104853_56007.png\" alt=\"\" />\r\n	</p>\r\n	<p class=\"MsoNormal\" style=\"color:#535353;font-family:\'Microsoft YaHei\';text-indent:21pt;background-color:#FFFFFF;\">\r\n		<span style=\"line-height:2;font-size:16px;\">“双三角”能源企业环保工作交流研讨会在集团总部</span><span style=\"line-height:2;font-size:16px;\">5</span><span style=\"line-height:2;font-size:16px;\">楼多功能厅召开，来自珠三角和长三角的四家地方能源电力企业代表齐聚一堂，针对电厂环保治理技术方向，运营经验以及政策导向等方面进行了深入地交流与探讨。广东粤电集团副总经理洪荣坤、浙能集团生产总监兼生产安全与技术管理部主任胡松如、上海申能集团副总经理须伟泉和深圳能源集团安全与生产管理部总经理马迎辉分别作出发言，介绍了各自集团的基本情况以及在环保方面的努力。</span>\r\n	</p>\r\n	<p class=\"MsoNormal\" style=\"color:#535353;font-family:\'Microsoft YaHei\';text-indent:21pt;background-color:#FFFFFF;\">\r\n		<span style=\"line-height:2;font-size:16px;\">广东粤电集团属国内较早实现下属电厂投运脱硫、脱硝等环保设施的能源集团之一。目前，集团下属所有燃煤机组都已投运脱硫、脱硝装置。得益于脱硫、脱硝和深度除尘等环保设施的投运，集团下属电厂排放指标均大幅下降，满足国家及省市要求。但值得注意的是，粤电集团目前燃煤机组主要位于广东省境内，随着国家和广东省的排放指标日趋严格，集团仍旧面临很大的减排压力，而经过几轮技术改造后的机组，节能减排潜力逐渐枯竭，未来如何面对环保挑战，仍需要不断探索和研究。</span>\r\n	</p>\r\n	<p class=\"MsoNormal\" style=\"color:#535353;font-family:\'Microsoft YaHei\';text-indent:21pt;background-color:#FFFFFF;\">\r\n		<span style=\"line-height:2;font-size:16px;\">浙能集团提早谋划、超前行动开展重点控排污染物治理工作。早在</span><span style=\"line-height:2;font-size:16px;\">2005</span><span style=\"line-height:2;font-size:16px;\">年，集团就制定了“十一五脱硫规划”，于</span><span style=\"line-height:2;font-size:16px;\">2009</span><span style=\"line-height:2;font-size:16px;\">年底全面完成了燃煤机组脱硫改造；</span><span style=\"line-height:2;font-size:16px;\">2008</span><span style=\"line-height:2;font-size:16px;\">年，集团制定了“十二五脱硝规划”，</span>&nbsp;<span style=\"line-height:2;font-size:16px;\">并于</span><span style=\"line-height:2;font-size:16px;\">2014</span><span style=\"line-height:2;font-size:16px;\">年</span><span style=\"line-height:2;font-size:16px;\">6</span><span style=\"line-height:2;font-size:16px;\">月</span><span style=\"line-height:2;font-size:16px;\">25</span><span style=\"line-height:2;font-size:16px;\">日完成了集团内最后一台机组的脱硝改造。目前集团正在积极推进燃煤机组超低排放技术改造，年中投运的嘉兴电厂</span><span style=\"line-height:2;font-size:16px;\">2</span><span style=\"line-height:2;font-size:16px;\">台超低排放示范机组通过国家、省、市等权威机构认证，主要污染物排放明显低于天然气机组排放限值，并可以长期稳定运行。预计到</span><span style=\"line-height:2;font-size:16px;\">2017</span><span style=\"line-height:2;font-size:16px;\">年底，集团所有</span><span style=\"line-height:2;font-size:16px;\">30</span><span style=\"line-height:2;font-size:16px;\">万千瓦及以上的机组均完成超低排放改造工作。</span>\r\n	</p>\r\n	<p class=\"MsoNormal\" style=\"color:#535353;font-family:\'Microsoft YaHei\';text-indent:21pt;background-color:#FFFFFF;\">\r\n		<span style=\"line-height:2;font-size:16px;\">申能集团依托科技创新和技术改造，不断加大节能减排投入，挖潜拓新，所属发电企业节能减排指标处于行业领先水平，其中外高桥第三发电公司燃煤机组能耗指标和排放指标处于国际领先，树立了中国煤电在国际发电领域的新形象。目前，集团所属电厂在投入脱硫、脱硝和深度除尘的基础上，积极开展燃煤电厂污染物深度治理技术的研究和试点示范工程，消除石膏雨、减少三氧化硫、可凝结颗粒物等污染物排放。此外，外高桥第三发电厂创新的“零能耗”脱硫系列技术和全天候脱硝技术成功解决了困扰国内同类型电厂设备运行的难题，为国内燃煤机组环保运行提供了新的技术思路。</span>\r\n	</p>\r\n	<p class=\"MsoNormal\" style=\"color:#535353;font-family:\'Microsoft YaHei\';text-indent:21pt;background-color:#FFFFFF;\">\r\n		<span style=\"line-height:2;font-size:16px;\">深能集团作为特区内的大型能源企业，在环保工作上也未雨绸缪、积极探索。</span><span style=\"line-height:2;font-size:16px;\">1999</span><span style=\"line-height:2;font-size:16px;\">年妈湾电厂率先建成全国首家海水烟气脱硫示范工程；河源电厂通过自主研发“蒸发</span><span style=\"line-height:2;font-size:16px;\">+</span><span style=\"line-height:2;font-size:16px;\">结晶”系统，在国内率先兑现“脱硫废水零排放”的承诺；而拥有自主知识产权的“垃圾渗滤液处理”技术已获得</span><span style=\"line-height:2;font-size:16px;\">5</span><span style=\"line-height:2;font-size:16px;\">项国家专利，并在多个垃圾焚烧发电厂中成功运用。目前，集团下属燃煤电厂已经投入脱硫、脱硝等环保设施，正在加紧深度除尘改造，各项排放指标满足国家及省市环保要求。</span>\r\n	</p>\r\n	<p class=\"MsoNormal\" style=\"color:#535353;font-family:\'Microsoft YaHei\';text-indent:21pt;background-color:#FFFFFF;\">\r\n		<span style=\"line-height:2;font-size:16px;\">深能集团副总裁秦士孝认为，现阶段燃煤电厂环保改造普遍面临相关标准更迭过快，改造潜力逐现瓶颈等困扰。近期国家三部委联合下发的《煤电节能减排升级与改造行动计划》为燃煤电厂未来几年的环保改造指明了方向。针对改造瓶颈问题，各电力集团也可以考虑与当地环保部门或研究机构合作，深入研究燃煤电厂与雾霾形成之间的关联，并加大宣传力度，引导社会对燃煤电厂的理性认识，为燃煤电厂赢得更大的生存空间；同时，燃煤电厂也需要坚持发展和运用环保技术，不断突破自身限制。借此机会，建议四家企业建立起定期交流、信息共享机制，促进各自环保工作迈向一个新的平台。</span>\r\n	</p>\r\n	<p class=\"MsoNormal\" style=\"color:#535353;font-family:\'Microsoft YaHei\';text-indent:21pt;background-color:#FFFFFF;\">\r\n		<span style=\"line-height:2;font-size:16px;\">会后，申能集团参会代表前往宝安垃圾发电厂参观、调研，对宝安垃圾发电厂的高标准运营管理水平和媲美欧盟的排放指标表示赞赏。粤电集团和浙能集团的代表则前往河源电厂，实地参观了世界上首例燃煤电厂“脱硫废水零排放”技术</span><span style=\"line-height:2;font-size:16px;\">。</span>\r\n	</p>\r\n	<p class=\"MsoNormal\" style=\"color:#535353;font-family:\'Microsoft YaHei\';text-indent:21pt;background-color:#FFFFFF;\">\r\n		<span style=\"line-height:2;font-size:14px;\"></span>\r\n	</p>\r\n	<p class=\"MsoNormal\" style=\"color:#535353;font-family:\'Microsoft YaHei\';text-indent:21pt;background-color:#FFFFFF;\">\r\n		<span style=\"line-height:2;font-size:14px;\"></span><img alt=\"\" src=\"http://www.sec.com.cn/Images/Upload/image/20140928/20140928142119_6565.jpg\" />\r\n	</p>\r\n	<p class=\"MsoNormal\" style=\"color:#535353;font-family:\'Microsoft YaHei\';text-indent:21pt;background-color:#FFFFFF;\">\r\n		<span style=\"line-height:2;font-size:14px;\"></span>\r\n	</p>\r\n	<p>\r\n		<br />\r\n	</p>\r\n</p>\r\n<p>\r\n	<br />\r\n</p>','本站',12,NULL,NULL,1417760472,1419477015,1,NULL,NULL,NULL),(10,'我司喜获万科广深区域“2013年度安全文明管理第一”荣誉称号我司喜获万科广深区域“2013年度安全文明管理第一”荣誉称号我司喜获万科广深区域“2013年度安全文明管理第一”荣誉称号我司喜获万科广深区域“2013年度安全文明管理第一”荣誉称号',NULL,'本站',12,NULL,NULL,1417760472,NULL,NULL,NULL,NULL,NULL),(11,'深圳中天精装股份有限公司改制更名暨揭牌仪式顺利进行',NULL,'本站',12,NULL,NULL,1417760472,NULL,NULL,NULL,NULL,NULL),(12,'我司荣获“广东省优秀建筑装饰工程奖”荣誉称号',NULL,'本站',12,NULL,NULL,1417760472,NULL,NULL,NULL,NULL,NULL),(14,'幻灯片1','<p>\r\n	<img src=\"/qtCMS/Public/uploads/image/20141208/20141208104301_88041.jpg\" alt=\"\" />\r\n</p>\r\n<p>\r\n	<img src=\"/qtCMS/Public/uploads/image/20141208/20141208104442_22859.jpg\" alt=\"\" />\r\n</p>','本站',21,NULL,1,1418006586,1419925894,1,NULL,NULL,NULL),(16,'2014年9月25日济宁展会','公司创建于2014年，为湖北正宏传媒集团的全资下属子公司。并且是享有国家政策扶持的再生资源开发利用企业，总公司位于武汉市东湖经济开发区鲁磨路，目前在武汉洪山区青王路设有大型的存储、分拣中心，并拥有多辆货运车辆。','本站',12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,'幻灯片','<img src=\"/qtCMS/Public/uploads/image/20141211/20141211141312_78884.jpg\" alt=\"\" />','本站',21,NULL,1,1418278398,1418278398,1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `qt_article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qt_attachment`
--

DROP TABLE IF EXISTS `qt_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qt_attachment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `article_id` int(10) DEFAULT NULL,
  `attach_type` tinyint(1) DEFAULT NULL COMMENT '类型： 1图片，2视频',
  `url` varchar(200) DEFAULT NULL COMMENT '文件路径',
  PRIMARY KEY (`id`),
  KEY `article_id` (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='文章附件';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qt_attachment`
--

LOCK TABLES `qt_attachment` WRITE;
/*!40000 ALTER TABLE `qt_attachment` DISABLE KEYS */;
INSERT INTO `qt_attachment` VALUES (3,15,1,'/qtCMS/Public/uploads/image/20141208/20141208104442_22859.jpg'),(7,17,1,'/qtCMS/Public/uploads/image/20141211/20141211141312_78884.jpg'),(8,8,1,'/qtCMS/Public/uploads/image/20141210/20141210104853_56007.png'),(9,8,1,'http://www.sec.com.cn/Images/Upload/image/20140928/20140928142119_6565.jpg'),(11,18,1,'/qtCMS/Public/uploads/image/20141230/20141230145852_50748.png'),(13,19,1,'/qtCMS/Public/uploads/image/20141230/20141230150741_21214.png'),(20,14,1,'/qtCMS/Public/uploads/image/20141208/20141208104301_88041.jpg'),(21,14,1,'/qtCMS/Public/uploads/image/20141208/20141208104442_22859.jpg');
/*!40000 ALTER TABLE `qt_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qt_category`
--

DROP TABLE IF EXISTS `qt_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qt_category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL COMMENT '父ID',
  `is_show` int(1) DEFAULT NULL COMMENT '是否显示1:显示 ，2不显示',
  `sort` int(3) DEFAULT NULL COMMENT '排序',
  `page_type` int(1) DEFAULT NULL COMMENT '1:单页面，2:多页面',
  `static_type` int(1) DEFAULT NULL COMMENT '1:不可修改和删除，2动态的',
  `target_tpl` varchar(100) DEFAULT NULL COMMENT '对应的模版',
  `target_detail_tpl` varchar(100) DEFAULT NULL COMMENT '多页面类型时对应的详细页面模版',
  `relation_model` varchar(20) DEFAULT NULL COMMENT '对应的模型model',
  `page_size` tinyint(4) DEFAULT NULL COMMENT '分页列表数',
  `open_comment` int(1) DEFAULT NULL COMMENT '1：开启评论  2：关闭评论',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='栏目分类';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qt_category`
--

LOCK TABLES `qt_category` WRITE;
/*!40000 ALTER TABLE `qt_category` DISABLE KEYS */;
INSERT INTO `qt_category` VALUES (6,'新闻咨询',0,1,2,2,1,NULL,NULL,'Article',NULL,NULL),(7,'公司概况',0,1,1,1,1,NULL,NULL,'Article',NULL,NULL),(8,'企业介绍',7,1,1,1,1,'single_company_profile',NULL,'Article',NULL,NULL),(9,'业务领域',7,1,2,1,1,'single_company_cultural',NULL,'Article',NULL,NULL),(10,'组织架构',7,1,3,1,1,'single_company_org',NULL,'Article',NULL,NULL),(11,'企业理念',7,1,4,1,1,'single_company_idea',NULL,'Article',NULL,NULL),(12,'企业新闻',6,1,1,2,1,'loop_news_company',NULL,'Article',10,NULL),(13,'行业动态',6,1,2,2,1,'loop_news_industry',NULL,'Article',NULL,NULL),(14,'电子期刊',6,2,3,2,1,'loop_news_elect',NULL,'Article',NULL,NULL),(15,'人力资源',0,1,3,1,1,NULL,NULL,'Article',NULL,NULL),(16,'人才战略',15,1,1,1,1,'single_job_talent',NULL,'Article',NULL,NULL),(17,'招聘信息',15,1,2,2,1,'loop_jobs',NULL,'Job',NULL,NULL),(18,'毛遂自荐',15,1,3,1,1,'single_job_form',NULL,'Article',NULL,NULL),(19,'联系我们',0,1,4,1,1,NULL,NULL,'Article',NULL,NULL),(20,'联系方式',19,1,1,1,1,'single_contact',NULL,'Article',NULL,NULL),(21,'幻灯片',0,2,5,2,NULL,NULL,NULL,'Article',NULL,NULL),(23,'最新产品',0,2,6,2,NULL,'loop_product',NULL,'Product',NULL,NULL),(24,'电子类',23,2,1,2,NULL,'loop_product',NULL,'Product',NULL,NULL);
/*!40000 ALTER TABLE `qt_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qt_comment`
--

DROP TABLE IF EXISTS `qt_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qt_comment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `relation_model` varchar(50) DEFAULT NULL COMMENT 'Article,Product',
  `relation_id` int(10) DEFAULT NULL COMMENT '被评论的ID',
  `content` varchar(1000) DEFAULT NULL COMMENT '评论内容',
  `public_time` int(11) DEFAULT NULL COMMENT '发布时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='评论';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qt_comment`
--

LOCK TABLES `qt_comment` WRITE;
/*!40000 ALTER TABLE `qt_comment` DISABLE KEYS */;
INSERT INTO `qt_comment` VALUES (1,NULL,'Article',3,'测试评论',NULL,NULL);
/*!40000 ALTER TABLE `qt_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qt_job`
--

DROP TABLE IF EXISTS `qt_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qt_job` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category_id` int(10) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT '职位名称',
  `address` varchar(50) DEFAULT NULL COMMENT '工作地点',
  `people_num` tinyint(4) DEFAULT NULL COMMENT '招聘人数',
  `content` mediumtext COMMENT '详情',
  `public_time` int(11) DEFAULT NULL COMMENT '发布时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `state` tinyint(1) DEFAULT NULL COMMENT '1：失效',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='招聘信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qt_job`
--

LOCK TABLES `qt_job` WRITE;
/*!40000 ALTER TABLE `qt_job` DISABLE KEYS */;
INSERT INTO `qt_job` VALUES (14,17,'设计工程师','wuhan',1,'发射点发烧fsafsadf',1417743927,1417745776,NULL),(15,17,'设计工程师1','武汉',2,'1233',1417743927,1417743927,NULL);
/*!40000 ALTER TABLE `qt_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qt_node`
--

DROP TABLE IF EXISTS `qt_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qt_node` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` int(11) NOT NULL COMMENT '父节点id',
  `name` varchar(32) NOT NULL COMMENT '节点名称',
  `title` varchar(32) NOT NULL COMMENT '节点标题',
  `level` tinyint(4) NOT NULL COMMENT '节点等级',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '节点状态',
  `created_at` int(11) NOT NULL COMMENT '创建时间',
  `updated_at` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qt_node`
--

LOCK TABLES `qt_node` WRITE;
/*!40000 ALTER TABLE `qt_node` DISABLE KEYS */;
/*!40000 ALTER TABLE `qt_node` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qt_product`
--

DROP TABLE IF EXISTS `qt_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qt_product` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  `tag` varchar(100) DEFAULT NULL COMMENT '标签',
  `price` double(10,2) DEFAULT NULL COMMENT '原价',
  `discount` double(10,2) DEFAULT NULL COMMENT '几折',
  `summary` mediumtext COMMENT '概述',
  `content` mediumtext COMMENT '详情',
  `category_id` int(10) DEFAULT NULL,
  `public_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `recommend` int(1) DEFAULT NULL COMMENT '1推荐 2不推荐',
  `has_picture` int(1) DEFAULT NULL COMMENT '1有产品图片， 2没有',
  `main_picture` varchar(100) DEFAULT NULL COMMENT '主图路径',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='产品';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qt_product`
--

LOCK TABLES `qt_product` WRITE;
/*!40000 ALTER TABLE `qt_product` DISABLE KEYS */;
INSERT INTO `qt_product` VALUES (1,'123',NULL,123.00,123.00,'123','123',24,1420700064,1420708516,2,1,'Public/uploads/product/54ae4a16cfb3f.png'),(6,'1234',NULL,0.00,0.00,'','',24,1420709084,1420788848,2,1,'Public/uploads/product/54ae4e0d8b429.png');
/*!40000 ALTER TABLE `qt_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qt_product_image`
--

DROP TABLE IF EXISTS `qt_product_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qt_product_image` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `path` varchar(150) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `type` int(1) DEFAULT NULL COMMENT '1:主图(封面)  2：附图',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COMMENT='产品图片';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qt_product_image`
--

LOCK TABLES `qt_product_image` WRITE;
/*!40000 ALTER TABLE `qt_product_image` DISABLE KEYS */;
INSERT INTO `qt_product_image` VALUES (57,'Public/uploads/product/54ae4a16b1414.png',1,2),(58,'Public/uploads/product/54ae4a16cfb3f.png',1,1),(59,'Public/uploads/product/54ae4c3194a62.png',5,1),(61,'Public/uploads/product/54ae4e0d8b429.png',6,1);
/*!40000 ALTER TABLE `qt_product_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qt_resume`
--

DROP TABLE IF EXISTS `qt_resume`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qt_resume` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) DEFAULT NULL,
  `sex` varchar(2) DEFAULT NULL COMMENT '男，女',
  `borth_year` varchar(4) DEFAULT NULL,
  `borth_month` varchar(2) DEFAULT NULL,
  `borth_day` varchar(2) DEFAULT NULL,
  `marry` varchar(4) DEFAULT NULL COMMENT '已婚，未婚',
  `education` varchar(20) DEFAULT NULL COMMENT '学历',
  `profession` varchar(50) DEFAULT NULL COMMENT '专业',
  `school` varchar(100) DEFAULT NULL COMMENT '毕业院校',
  `job_name` varchar(20) DEFAULT NULL COMMENT '应聘岗位',
  `transfer_job` varchar(2) DEFAULT NULL COMMENT '能否调岗: 能,否',
  `hope_job_address` varchar(50) DEFAULT NULL COMMENT '期望工作地',
  `salary` varchar(10) DEFAULT NULL COMMENT '期望薪酬',
  `tel` varchar(50) DEFAULT NULL COMMENT '联系电话',
  `qq_email` varchar(100) DEFAULT NULL COMMENT 'QQ/E-mail',
  `address` varchar(200) DEFAULT NULL COMMENT '家庭住址',
  `education_career` varchar(1000) DEFAULT NULL COMMENT '教育经历',
  `job_career` varchar(2000) DEFAULT NULL COMMENT '工作经历',
  `read_tag` varchar(10) DEFAULT NULL COMMENT '未读，已读',
  `remark` varchar(500) DEFAULT NULL COMMENT '补充备注',
  `photo` varchar(200) DEFAULT NULL COMMENT '图片路径',
  `public_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='简历';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qt_resume`
--

LOCK TABLES `qt_resume` WRITE;
/*!40000 ALTER TABLE `qt_resume` DISABLE KEYS */;
INSERT INTO `qt_resume` VALUES (1,'12','男','2014','01','01','已婚','23','13','13','13','能','3123','fsadf','15827564280','','2313','123','123132','已读',NULL,NULL,1419402157),(2,'123','男','1955','4','4','已婚','23','斯蒂芬森','发送发顺丰撒发顺丰','发','能','','1444','027-87987866','','12','23','123','已读','不符合要求',NULL,1419404069);
/*!40000 ALTER TABLE `qt_resume` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qt_role`
--

DROP TABLE IF EXISTS `qt_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qt_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` int(11) NOT NULL COMMENT '父角色id',
  `name` varchar(32) NOT NULL COMMENT '角色名称',
  `description` text NOT NULL COMMENT '角色描述',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '角色状态',
  `created_at` int(11) NOT NULL COMMENT '创建时间',
  `updated_at` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='角色表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qt_role`
--

LOCK TABLES `qt_role` WRITE;
/*!40000 ALTER TABLE `qt_role` DISABLE KEYS */;
INSERT INTO `qt_role` VALUES (1,0,'超级管理员','系统内置超级管理员组，不受权限分配账号限制',1,1415234982,1415234982),(2,1,'管理员','',1,1415253151,1415253160);
/*!40000 ALTER TABLE `qt_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qt_role_admin`
--

DROP TABLE IF EXISTS `qt_role_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qt_role_admin` (
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `user_id` int(11) NOT NULL COMMENT '管理员id',
  KEY `fk_role_admin` (`role_id`),
  KEY `fk_admin_role` (`user_id`),
  CONSTRAINT `fk_ar` FOREIGN KEY (`user_id`) REFERENCES `qt_admin` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ra` FOREIGN KEY (`role_id`) REFERENCES `qt_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员权限表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qt_role_admin`
--

LOCK TABLES `qt_role_admin` WRITE;
/*!40000 ALTER TABLE `qt_role_admin` DISABLE KEYS */;
INSERT INTO `qt_role_admin` VALUES (1,1);
/*!40000 ALTER TABLE `qt_role_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qt_siteinfo`
--

DROP TABLE IF EXISTS `qt_siteinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qt_siteinfo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `seo_keywords` varchar(200) DEFAULT NULL,
  `seo_description` varchar(500) DEFAULT NULL,
  `icp` varchar(100) DEFAULT NULL,
  `weixin` varchar(200) DEFAULT NULL,
  `weibo` varchar(200) DEFAULT NULL,
  `twocode` varchar(200) DEFAULT NULL COMMENT '二维码',
  `company_name` varchar(200) DEFAULT NULL,
  `company_address` varchar(200) DEFAULT NULL,
  `company_tel` varchar(50) DEFAULT NULL,
  `company_fox` varchar(50) DEFAULT NULL,
  `company_website` varchar(100) DEFAULT NULL,
  `company_email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='seo,icp';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qt_siteinfo`
--

LOCK TABLES `qt_siteinfo` WRITE;
/*!40000 ALTER TABLE `qt_siteinfo` DISABLE KEYS */;
INSERT INTO `qt_siteinfo` VALUES (2,'湖北绿之洲再生资源有限公司','湖北绿之洲再生资源有限公司','鄂ICP备06084595号','12345','12345','Public/uploads/twocode/5493c04ca2f08.png','湖北绿之洲再生资源有限公司','湖北省武汉市洪山区东湖高新技术开发区鲁磨路','027-12345678','027-87654321','www.lzzrr.com','lvzhizhou@163.com');
/*!40000 ALTER TABLE `qt_siteinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qt_user`
--

DROP TABLE IF EXISTS `qt_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qt_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `sex` varchar(4) DEFAULT NULL COMMENT '男，女',
  `reallyname` varchar(30) DEFAULT NULL COMMENT '真实姓名',
  `tel` varchar(30) DEFAULT NULL COMMENT '电话',
  `photo` varchar(100) DEFAULT NULL COMMENT '头像',
  `last_logintime` int(11) DEFAULT NULL COMMENT '登录时间',
  `public_time` int(11) DEFAULT NULL COMMENT '注册时间',
  `update_time` int(11) DEFAULT NULL COMMENT '修改时间',
  `state` int(1) DEFAULT NULL COMMENT '审核状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qt_user`
--

LOCK TABLES `qt_user` WRITE;
/*!40000 ALTER TABLE `qt_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `qt_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-01-12 17:16:28
